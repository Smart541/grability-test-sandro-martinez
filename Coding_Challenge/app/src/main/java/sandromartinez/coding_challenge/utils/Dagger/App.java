package sandromartinez.coding_challenge.utils.Dagger;

import android.app.Application;
import sandromartinez.coding_challenge.utils.Dagger.componentes.AppComponent;
import sandromartinez.coding_challenge.utils.Dagger.componentes.DaggerAppComponent;
import sandromartinez.coding_challenge.utils.Dagger.modulos.AppModule;

/**
 * Created by HostSandro on 6/6/2017.
 */

public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent (){
        return appComponent;
    }
}
