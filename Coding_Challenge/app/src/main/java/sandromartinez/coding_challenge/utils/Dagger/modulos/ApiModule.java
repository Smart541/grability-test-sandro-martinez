package sandromartinez.coding_challenge.utils.Dagger.modulos;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sandromartinez.coding_challenge.utils.Constant;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

@Module
public class ApiModule {
    @Provides
    @Singleton
    public Retrofit retrofit(){

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.URL_SERVER)
                .build();
    }
}
