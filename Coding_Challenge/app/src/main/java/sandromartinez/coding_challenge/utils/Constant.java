package sandromartinez.coding_challenge.utils;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

public interface Constant {

    String API_KEY = "eb17e443ae62c9771628ab295be96a17";
    String URL_SERVER = "https://api.themoviedb.org/3/";

    int TOOLBAR_STANDARD = 0;
    int TOOLBAR_DETAIL = 1;
}
