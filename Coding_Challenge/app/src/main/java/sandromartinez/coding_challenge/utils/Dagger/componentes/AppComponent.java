package sandromartinez.coding_challenge.utils.Dagger.componentes;

import javax.inject.Singleton;
import dagger.Component;
import sandromartinez.coding_challenge.utils.Dagger.modulos.ApiModule;
import sandromartinez.coding_challenge.utils.Dagger.modulos.AppModule;
import sandromartinez.coding_challenge.vistas.base.BaseFragment;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)

public interface AppComponent {

        void inject(BaseFragment basefragment);

}
