package sandromartinez.coding_challenge.vistas.base;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import sandromartinez.coding_challenge.R;
import sandromartinez.coding_challenge.utils.Constant;
import sandromartinez.coding_challenge.utils.Dagger.componentes.AppComponent;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

public abstract class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();
    private boolean hasToolbar;
    private Toolbar mToolbar;
    private int typeToolbar = Constant.TOOLBAR_STANDARD;
    private BaseActivity activity;
    private Context context;

    abstract public int getLayoutView();

    abstract public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                                     @Nullable Bundle savedInstanceState, View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View view = inflater.inflate(getLayoutView(), container, false);
        ((BaseActivity) getActivity()).getAppComponent().inject(this);
        getApplicationComponent().inject(this);
        if (hasToolbar) {
            onCreateToolbar(view , typeToolbar);
        }
        ButterKnife.bind(this, view);
        onViewReady(inflater, container, savedInstanceState, view);
        return view;
    }

    public AppComponent getApplicationComponent() {
        return this.activity.getAppComponent();
    }

    protected void setTitle(String title) {
        if (hasToolbar) {
            mToolbar.setTitle("");
            mToolbar.setSubtitle("");
            //tvTitle.setText(title);
        } else {
            activity.setTitle(title);
        }
    }

    public void pushFragment(DialogFragment dialogFragment){
        activity.pushFragment(dialogFragment);
    }

    public void onCreateToolbar(View view, int typeToolbar) {
        try {
            switch (typeToolbar){
                case Constant.TOOLBAR_STANDARD:
                    Log.d("-->","standar");
                    //todo mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
                    break;
                case Constant.TOOLBAR_DETAIL:
                    Log.d("-->","detail");
                    //todo mToolbar = (Toolbar) view.findViewById(R.id.toolbar_detail);
                    break;
            }
            //tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            setToolBar(mToolbar);
        } catch (Exception e) {
            setHasToolbar(false);
        }
    }
    public void setToolBar(Toolbar toolBar) {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if (baseActivity != null) {
            mToolbar = toolBar;
            ((BaseActivity) getActivity()).setSupportActionBar(toolBar);
            activity.hideToolbar();
            setHasToolbar(true);
        }
    }

    public void setHasToolbar(boolean hasToolbar, int typeToolbar) {
        setHasToolbar(hasToolbar);
        this.typeToolbar = typeToolbar;
    }

    public void setHasToolbar(boolean hasToolbar) {
        this.hasToolbar = hasToolbar;
    }

    public void setEnableBackToolbar(boolean enable) {
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(enable);
            if (mToolbar != null && enable){
                //todo mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            }
        }
    }

    public void simpleToast(String msg) {
        activity.simpleToast(msg);
    }

    public boolean onBackToolbar() {
        return false;
    }

    public boolean onBackPressed() {
        return false;
    }

    protected void goBack() {
        activity.goBack();
    }
}
