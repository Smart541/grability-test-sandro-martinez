package sandromartinez.coding_challenge.vistas.modulos.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sandromartinez.coding_challenge.R;
import sandromartinez.coding_challenge.utils.Constant;
import sandromartinez.coding_challenge.vistas.base.BaseFragment;

public class MainFragment extends BaseFragment {

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }


    @Override
    public int getLayoutView() {
        return R.layout.fragment_main;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("-->","Pase por aki");
        setHasToolbar(true , Constant.TOOLBAR_STANDARD);
        //ToDo Definir tipos de Toolbar
        setHasOptionsMenu(true);
        //ToDo definir tipos de menu
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {

    }


}
