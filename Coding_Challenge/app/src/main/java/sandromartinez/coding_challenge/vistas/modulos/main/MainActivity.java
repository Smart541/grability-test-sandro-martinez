package sandromartinez.coding_challenge.vistas.modulos.main;

import android.os.Bundle;
import android.widget.FrameLayout;
import butterknife.BindView;
import sandromartinez.coding_challenge.R;
import sandromartinez.coding_challenge.vistas.base.BaseActivity;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

public class MainActivity extends BaseActivity {

    @BindView(R.id.Fragment_Container)
    FrameLayout Fragment_Container;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        pushFragment(MainFragment.newInstance(),false);
    }

}
