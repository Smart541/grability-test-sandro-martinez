package sandromartinez.coding_challenge.vistas.base;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;
import com.splunk.mint.Mint;
import java.util.ArrayList;
import butterknife.ButterKnife;
import sandromartinez.coding_challenge.R;
import sandromartinez.coding_challenge.utils.Dagger.App;
import sandromartinez.coding_challenge.utils.Dagger.componentes.AppComponent;

/**
 * Created by Sandro Martinez on 6/6/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private final String TAG = BaseActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private boolean hasToolbar;
    private DrawerLayout mDrawerLayout;

    protected FragmentManager FragMan;
    protected ArrayList<String> mTagFragments;

    abstract public int getLayout();
    abstract public void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragMan = getSupportFragmentManager();
        mTagFragments = new ArrayList<>();
        setContentView(getLayout());
        ButterKnife.bind(this);
        onCreateView(savedInstanceState);
        Mint.initAndStartSession(this.getApplication(), "0f4d2593");
    }

    @Override
    public void onBackPressed() {
        if (mTagFragments.size() > 0) {
            Fragment fragment = FragMan.findFragmentByTag(mTagFragments.get(mTagFragments.size() - 1));
            if (fragment instanceof BaseFragment) {
                BaseFragment base = (BaseFragment) fragment;
                if (base.onBackPressed()) {
                    return;
                }
            }
            mTagFragments.remove(mTagFragments.size() - 1);
        }
        super.onBackPressed();
    }

    public AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    public void showFragment(Fragment fragment, boolean backStack) {
        showFragment(fragment, R.id.Fragment_Container, backStack);
    }

    public void showFragment(Fragment fragment, int container, boolean addBackStack) {
        FragmentTransaction transaction = FragMan.beginTransaction();
        String tag = fragment.getClass().getSimpleName();

        if (addBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        transaction.add(container, fragment, tag);
        try {
            transaction.commit();
        } catch (Exception e) {
            return;
        }
        if((mTagFragments.size() == 0 )|| addBackStack){
            mTagFragments.add(tag);
        }
    }

    public void pushFragment(Fragment fragment, boolean backStack) {
        pushFragment(fragment, R.id.Fragment_Container, backStack);
    }

    public void pushFragment(Fragment fragment, int container, boolean addBackStack) {
        FragmentTransaction transaction = FragMan.beginTransaction();
        String tag = fragment.getClass().getSimpleName();

        if (addBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        transaction.replace(container, fragment, tag);
        try {
            transaction.commit();
        } catch (Exception e) {
            return;
        }
        if((mTagFragments.size() == 0 )|| addBackStack){
            mTagFragments.add(tag);
        }
    }

    //Push fragment para Dialogs

    public void pushFragment(DialogFragment dialogFragment){
        dialogFragment.show(getFragmentManager(),dialogFragment.getTag());
    }

    public void pushFragment(DialogFragment dialogFragment, boolean cancelable){
        dialogFragment.setCancelable(cancelable);
        dialogFragment.show(getFragmentManager(),dialogFragment.getTag());
    }

    public void setToolbar(Toolbar toolBar) {
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            mToolbar = toolBar;
            hasToolbar = true;
        }
    }

    public void showToolbar() {
        if ((getSupportActionBar() != null) && hasToolbar) {
            setToolbar(mToolbar);
            mToolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideToolbar() {
        if ((getSupportActionBar() != null) && hasToolbar) {
            mToolbar.setVisibility(View.GONE);
        }
    }

    public void closeDrawer(){
        if(this.mDrawerLayout!=null&&isOpenDrawerLayout()){
            //this.mDrawerLayout.closeDrawer(mDrawerList);
            this.mDrawerLayout.closeDrawers();
        }
    }

    public void openDrawer(){
        if(mDrawerLayout!=null&&!isOpenDrawerLayout())
            this.mDrawerLayout.openDrawer(Gravity.START);
            //this.mDrawerLayout.openDrawer(this.mDrawerList);
    }

    public boolean isOpenDrawerLayout(){
        return mDrawerLayout.isActivated();
    }

    public void simpleToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void goBack() {
        if (mTagFragments.size() > 0) {
            mTagFragments.remove(mTagFragments.size() - 1);
            FragMan.popBackStackImmediate();
        }
    }

}
